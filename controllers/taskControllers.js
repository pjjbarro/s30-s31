//import Task model
const Task = require('../models/Task');

module.exports.createTaskController = (req,res)=>{

	console.log(req.body);
	//Create a new Task document
		//Check if there is a document with duplicate name field
		//Model.findOne() is a Mongoose method similar to findOne in MongoDB
		//However, with Model.findOne(), we can process the result via our API.
		Task.findOne({name : req.body.name},(err,result)=>{

			console.log(err);//returns null if no error
			console.log(result);//returns null if no documents were found with the name given in the criteria.

			//This will allow us to send a message to the client a duplicate document was found.
			if(result !== null && result.name === req.body.name){

				return res.send("Duplicate Task Found");

			} else {

					//Created a new task object out of our Task model.
					//newTask has added methods for use in our application.
					let newTask = new Task({

						name: req.body.name

					})

					//.save() is a method from an object created by a model.
					//This will then allow us to save our document into our collection.
					//.save() can have an anonymous function and this can take 2 parameters
					//The first item: saveErr receives an error object if there was an creating our document.
					//the second item is our saved document.
					newTask.save((saveErr,savedTask)=>{

						console.log(savedTask);

						if(saveErr){

							return console.error(saveErr);

						} else {

							return res.send("New Task Created");

						}

					})


			}

		})
		//then () is able to capture the reult of our query
		//.then() is able to process the result and when that result is returned, we can actually add another then() to process the next result.
		//.catch () is able to capture the error of our query
		.then(result =>) {

		.catch(err => res.send(err));
}

module.exports.getAllTasksController = (req,res)=>{

	//Model.find() is a Mongoose method similar to MongoDB's find(). It is able to retrieve all documents that will match the criteria. 
	Task.find({})
	.then(result =>res.send(result))
	.catch(err=> res.send(err))

}

module.exports.getAllTasksController = (req,res) =>{

	console.log(req.params);
	//mongoose queries such as Model.find(),Model.findOne(), Model.findById() has a second argument for projection. And, how it works is the same as in mongoDB
	Task.findByid(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.completeSingleTasksController = (req,res) =>{
	let updates = {
		status: "complete"
	}
	Task.findByIdAndUpdate(req.params.id,updates,{new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err))
}
module.exports.cancelSingleTasksController = (req,res) =>{

	let updates = {
		status: "cancelled"
	}
	Task.findByIdAndUpdate(req.params.id,updates,{new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err))
}