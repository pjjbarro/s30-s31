//Mini-Activity

const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 4000;
//change myFirstDatabase to todoList123
//MongoDB upon connection will create the todoList123 db once we created documents for it.
mongoose.connect("mongodb+srv://Phyro10:Teambahay123@cluster0.iuuue.mongodb.net/todoList123?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
//notifications if connection to db is success or failed.
let db = mongoose.connection;
//console.error.bind(console,<<message>) - print error in both terminal and browser
db.on("error",console.error.bind(console, "connection error."))
//Output a message in the terminal if the connection is successful
db.once("open", ()=> console.log("Connected to MongoDB"));

//Middleware - middleware, in expressjs context, are methods, functions that acts and adds features to your application
//handle json data from our client
app.use(express.json())

const taskRoutes = require('./routes/taskRoutes');
console.log(taskRoutes);
//A middleware to group all our routes starting their endpoints with /tasks
app.use('/tasks',taskRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);





app.get('/hello',(req,res)=>{

	res.send('Hello from our new Express Api!')
})

app.listen(port,()=>console.log('Server running at port ${port'))