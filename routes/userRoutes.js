//import express and Router()
const express = require('express');
const router = express.Router();

//import the user controllers
const userControllers = require('../controllers/userControllers')
const {createUserController,getAllusersController} = userControllers

router.post('/', createUserController);


router.get('/', getAllusersController);

//get single user route
router.put('/:id')

//export router which contains our routes
module.exports= router;