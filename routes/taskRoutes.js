const express = require('express');
//Router() from express, allows us the access to HTTP method routes.
//Router() will act as a middleware and our routing system.
const router = express.Router();
//Routes should only be concerned with our endpoints and our methods.
//The action to be done once a route is accessed should be in separate file, it should be in our controllers.
const taskControllers = require('../controllers/taskControllers');
const {createTaskController,getAllTasksController} = taskControllers
router.post('/tasks', createTaskController);
router.get('/tasks', getAllTasksController);

//Mini-Activity
route.get('/:id');


route.put('/complete/:id',completeSingleTaskController);

route.put('/cancel/:id',cancelSingleTaskController);



//router holds all of our routes and can be exported and imported into another file.
module.exports = router;